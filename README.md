# LSE IM

#### 介绍
LSE IM 即时通讯系统 web网页休闲型聊天系统 - 后端代码

前端仓库地址：[LSE IM Page: LSE IM 前端页面 - Gitee.com](https://gitee.com/LSE_QX/lse-im-page)

#### 软件架构
本项目采用spring boot + elasticsearch 7.9.3 + rabbitmq + redis + mysql 8.0

使用本项目源码请安装以下依赖

**elasticsearch**（本项目采用7.9.3版本）：

elasticsearch-analysis-ik-7.9.3 （ik分词器插件）

elasticsearch-analysis-pinyin-7.9.3 (拼音插件)

elasticsearch-analysis-stconvert-7.9.3（中文简体字插件）

**rabbitmq** （本项目采用3.8.9版本）

**redis** （本项目采用3.2.1版本）

PS：请注意 redis需要设置用户名密码等 也可在项目源代码种自行更改

PS：本项目采用了多数据源配置  数据源为 lse与lse_im_app 其中lse为其他项目的主数据源

